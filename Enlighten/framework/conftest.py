import pytest
from selenium import webdriver
from framework.LoginPage import Login
from framework.LogoutPage import Logout
from framework import FileUtil
@pytest.fixture()
def setup():
          driver = webdriver.Chrome()
          driver.maximize_window()
          driver.implicitly_wait(5)
          driver.get('https://online.actitime.com/test123/login.do')
          username=FileUtil.readData(0,1,0)
          password = FileUtil.readData(0, 1, 1)
          loginPage = Login(driver)
          loginPage.loginToApp(username,password)

          yield

          print('Logout to Appliction')
          logoutpage=Logout(driver)
          logoutpage.logoutToApp()
          driver.close()


